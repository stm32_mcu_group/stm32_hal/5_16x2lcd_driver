//
// Created by Csurleny on 3/18/2022.
//

#include "lcd.h"
#include "main.h"

#define CLEAR_DISPLAY 0x01

#define RETURN_HOME 0x02

#define ENTRY_MODE_SET 0x04
#define OPT_S	0x01					// Shift entire display to right
#define OPT_INC 0x02					// Cursor increment

#define DISPLAY_ON_OFF_CONTROL 0x08
#define OPT_D	0x04					// Turn on display
#define OPT_C	0x02					// Turn on cursor
#define OPT_B 	0x01					// Turn on cursor blink

#define CURSOR_DISPLAY_SHIFT 0x10		// Move and shift cursor
#define OPT_SC 0x08
#define OPT_RL 0x04

#define FUNCTION_SET 0x20
#define OPT_DL 0x10						// Set interface data length
#define OPT_N 0x08						// Set number of display lines
#define OPT_F 0x04						// Set alternate font
#define SETCGRAM_ADDR 0x040
#define SET_DDRAM_ADDR 0x80				// Set DDRAM address

extern TIM_HandleTypeDef htim6;

static void delay(uint16_t us){
    __HAL_TIM_SET_COUNTER(&htim6, 0);
    while(__HAL_TIM_GET_COUNTER(&htim6) < us);
}

static void send_to_lcd(char data, int rs){
    HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, rs);

    uint32_t odr = GPIOC->ODR;
    odr |= (0x0000000F & data);
    odr &= (0xFFFFFFF0 | data);
    GPIOC->ODR = odr;

    HAL_GPIO_WritePin(LCD_EN_GPIO_Port, LCD_EN_Pin, 1);
    delay(20);
    HAL_GPIO_WritePin(LCD_EN_GPIO_Port, LCD_EN_Pin, 0);
    delay(20);
}

void lcd_send_cmd(char cmd){
    char temp_to_send;

    temp_to_send = ((cmd >> 4) & 0x0F);
    send_to_lcd(temp_to_send, 0);

    temp_to_send = ((cmd) & 0x0F);
    send_to_lcd(temp_to_send, 0);
}

void lcd_send_data(char data){
    char temp_to_send;

    temp_to_send = ((data >> 4) & 0x0F);
    send_to_lcd(temp_to_send, 1);

    temp_to_send = ((data) & 0x0F);
    send_to_lcd(temp_to_send, 1);
}

void lcd_clear(void){
    lcd_send_cmd(0x01);
    HAL_Delay(2);
}

void lcd_put_cur(int row, int col){
    switch(row){
        case 0:
            col |= 0x80;
            break;
        case 1:
            col |= 0xC0;
            break;
    }
    lcd_send_cmd(col);
}

void lcd_init(void){
    HAL_Delay(50);
    lcd_send_cmd(0x33);
    HAL_Delay(5);
    lcd_send_cmd(0x32);
    HAL_Delay(1);
    lcd_send_cmd(FUNCTION_SET | OPT_N);
    HAL_Delay(10);
    lcd_send_cmd(CLEAR_DISPLAY);
    HAL_Delay(1);
    lcd_send_cmd(DISPLAY_ON_OFF_CONTROL | OPT_D);
}

void lcd_send_string (char *str){
    while (*str) lcd_send_data (*str++);
}