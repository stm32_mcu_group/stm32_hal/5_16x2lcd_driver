//
// Created by Csurleny on 3/18/2022.
//

#ifndef INC_5_LCD_16X2_LCD_H
#define INC_5_LCD_16X2_LCD_H

void lcd_init(void);
void lcd_send_cmd (char cmd);
void lcd_send_data (char data);
void lcd_send_string (char *str);
void lcd_put_cur(int row, int col);
void lcd_clear (void);

#endif //INC_5_LCD_16X2_LCD_H
